

Installation
############

Choix BIOS / UEFI : attention, virtualbox gère mal l'UEFI. Utiliser BIOS si 
installation d'un guest Gentoo.

Activer CCACHE pour accélérer les temps de compilation
Voir : https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Features

1. Installer : 

.. code-block:: bash

	$ emerge --ask dev-util/ccache

2. Configurer /etc/portage/make.conf

Utilisation de Xorg pour utilisateur non root avec OpenRC
Voir https://wiki.gentoo.org/wiki/Non_root_Xorg
Et : https://www.gentoo.org/support/news-items/2020-06-24-xorg-server-dropping-default-suid.html
Par rapport au profil "desktop", on peut retirer bluetooth et mesh. Si on 
compte utiliser openrc et pas systemd, alors, il faut penser à vérifier que le 
flag "elogind" est bien activé dans la variable USE. On peut activer elogind 
au boot : 

.. code-block:: bash

	rc-update add elogind boot


Kernel settings
- Xorg 
- Carte graphique Nvidia GeForce 1050ti : https://wiki.gentoo.org/wiki/NVIDIA/nvidia-drivers
- Virtualbox guest : https://wiki.gentoo.org/wiki/VirtualBox#Gentoo_guests


Post installation
#################

Le paquet rust met enormement de temps à compiler, il peut etre intéressant de 
le remplacer par le binaire associé précompilé. Voir : 
https://wiki.gentoo.org/wiki/User:Vazhnov/Knowledge_Base:replace_rust_with_rust-bin

.. code-block:: bash

    $ emerge --ask --unmerge dev-lang/rust
    $ emerge --ask --oneshot virtual/rust dev-lang/rust-bin
    $ echo 'dev-lang/rust' >> /etc/portage/package.mask

Mise à jour complète post install
Voir https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Portage

.. code-block:: bash

    $ emerge --update --deep --with-bdeps=y --newuse @world
    $ emerge --depclean

Commandes pour libérer de la place

.. code-block:: bash

    $ eclean packages
    $ eclean-dist
    $ rm -r /var/cache/distfiles/*
    $ rm -r /var/tmp/portage/*

Placer le flag xft pur les polices de urxvt

.. code-block:: bash

    $ echo "x11-terms/rxvt-unicode xft" >> /etc/portage/package.use

Paquets à installer post install : 

.. code-block:: bash

    $ emerge --ask x11-base/xorg-server
    $ emerge --ask x11-misc/i3lock x11-wm/i3 x11-misc/dmenu x11-misc/i3status \
        app-admin/sudo x11-terms/rxvt-unicode x11-misc/urxvt-perls dev-vcs/git \
        app-shells/zsh app-shells/zsh-completions media-gfx/feh x11-misc/xss-lock \
        app-shells/gentoo-zsh-completions app-editors/vim x11-terms/xterm \
        media-fonts/terminus-font media-fonts/dejavu x11-misc/numlockx \ 
        app-portage/gentoolkit

    $ emerge --ask www-client/firefox-bin

    $ wget https://picsum.photos/1920/1200 -O wallpaper.jpg

Si virtualbox guest

.. code-block:: bash

    $ emerge --ask app-emulation/virtualbox-guest-additions
    $ rc-update add virtualbox-guest-additions default
    $ rc-update add dbus default

Déploiement des fichiers de config avec git

.. code-block:: bash

    $ git config --global user.email "and0uille@zaclys.net" 
    $ git config --global user.name "and0uille"
    $ temp=$(mktemp -d)
    $ git clone https://framagit.org/and0uille/Dotfile.git $temp
    $ cp $temp/Xresources ~/.Xresources
    $ cp $temp/xinitrc ~/.xinitrc

Verifier xinitrc

Ajout "Option "XkbLayout" "fr"" dans /sur/share/X11/xorg.conf.d/40-libinput.conf dans le bloc qui concerne les claviers.

Configuration de logiciels : 

- Xorg
- RXVT
- ZSH 
	* Ajout 'zsh-completion' à USE dans /etc/portage/make.conf
	* chsh -s /bin/zsh
- I3
 

Other tools
###########

- eix

.. code-block:: bash

    emerge --ask app-portage/eix
    eix-update


Mettre à jour les paquets
#########################

Voir : 
- https://wiki.gentoo.org/wiki/Handbook:AMD64/Working/Portage
- TODO

1. Update the Gentoo reposiroty 

.. code-block:: bash

    $ emerge --sync

2. Mettre à jour 

.. code-block:: bash

    $ emerge --update --deep --with-bdeps=y @world
    $ emerge --update --deep --with-bdeps=y --newuse @world
    $ emerge -u		-D			-nav

3. Nettoyer les dépendances orphelines

.. code-block:: bash

    $ emerge --ask --depclean

4. Vérifier les dépendances inverses eventuellement cassées par la MAJ

.. code-block:: bash

    $ revdep-rebuild -v -- --ask

4. Nettoyer les sources telechargées

.. code-block:: bash

    $ eclean-dist 

5. Merge des fichiers de confs mis à jour

.. code-block:: bash

    $ dispatch-conf


Mettre a jour / modifier le kernel
##################################

.. code-block:: bash

    $ cd /usr/src/linux
    $ make menuconfig
    $ make
    $ make modules_install
    $ make install
    $ grub-mkconfig -o /boot/grub/grub.cfg
    $ reboot
    $ emerge @module-rebuild


Mettre à jour le profile
########################


Voir : https://wiki.gentoo.org/wiki/Upgrading_Gentoo


Mise à jour de la toolchain
###########################

Voir : 

  - https://serverfault.com/questions/9936/optimal-procedure-to-upgrade-gentoo-linux
  - https://forums.gentoo.org/viewtopic-t-1118276-start-0.html
  - https://www.funtoo.org/Toolchain_update
  - https://abi-laboratory.pro/?view=timeline&l=glibc
  - https://www.linuxquestions.org/questions/linux-from-scratch-13/upgrading-the-toolchain-4175554894/




